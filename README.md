# README #

# El Centro

#### Installation

1. This site is running on WP-Engine this repo is NOT tracking Wordpress core files. Here is a good guide for setting up Git Push on Wp-engine.

https://wpengine.com/git/

#### Installation for Dev

1. Switch to El Centro theme www/wp-content/themes/elcentro
2. Run `npm install && bower install` to install development tools
3. Run `grunt` to automate SASS & JS pre-processing and minification, etc.

#### Pushing Code

1. This site (staging and production) is running on WP-Engine the repo is NOT tracking Wordpress core files (see .gitignore file). Our development flow is a bit different than most FPCS sites. WP-Engine handles all updates to core files, we only push Theme and Plugins. Push to WP-Engine staging server (not master) to test your code changes. Once code has been tested and approved there clone it to production from the WP-Admin user panel. Please try to follow these best practices (DATABASE MOVES DOWN, CODE MOVES UP) to keep Wordpress in sync.

https://wpengine.com/support/development-workflow-best-practices/

Pipelines have not been set up yet, so there are 2 repos WP-Engine and Bitbucket. Make yourself an endpoint for WP Engine.

#### Helpful WP Engine Articles

https://wpengine.com/support/git/

https://www.billerickson.net/git-push-wpengine/