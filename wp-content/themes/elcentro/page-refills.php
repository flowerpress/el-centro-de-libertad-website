<?php get_header(); ?>

<div class="hero parallax">
	<div id="js-parallax-window" class="parallax-window">
	  <div class="parallax-static-content">
	    <h1>Parallax Header</h1>
	  </div>
	  <div id="js-parallax-background" class="parallax-background"></div>
	</div>
</div>

<!-- begin content -->
<section class="doc-content subpage full">
	<div>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<article id="page-content" role="article" <?php post_class() ?> id="Post-<?php the_ID(); ?>">
				<?php the_content(__('(more...)')); ?>		
				
<h2>Expander</h2>

<div class="expander">
  <a href="javascript:void(0)" id="js-expander-trigger" class="expander-trigger expander-hidden">Expandable section</a>
  <div id="js-expander-content" class="expander-content">
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio mollitia fugiat facilis enim accusamus quisquam aut, repellendus incidunt quod optio facere labore illo numquam ipsum beatae vero debitis, fugit excepturi.</p>
  </div>
</div>


<h2>Accordion</h2>

<ul class="accordion">
  <li>
    <a href="javascript:void(0)" class="js-accordion-trigger">Accordion Item</a>
    <ul class="submenu">
      <li>
        <a href="javascript:void(0)">Sub Item 1</a>
      </li>
      <li>
        <a href="javascript:void(0)">Sub Item 2</a>
      </li>
    </ul>
  </li>
  <li>
    <a href="javascript:void(0)" class="js-accordion-trigger">Another Item</a>
    <ul class="submenu">
      <li>
        <a href="javascript:void(0)">Sub Item 1</a>
      </li>
      <li>
        <a href="javascript:void(0)">Sub Item 2</a>
      </li>
    </ul>
  </li>
</ul>


<h2>Accordion Tabs</h2>

<ul class="accordion-tabs">
  <li class="tab-header-and-content">
    <a href="javascript:void(0)" class="is-active tab-link">Tab Item</a>
    <div class="tab-content">
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tincidunt pellentesque lorem, id suscipit dolor rutrum id. Morbi facilisis porta volutpat. Fusce adipiscing, mauris quis congue tincidunt, sapien purus suscipit odio, quis dictum odio tortor in sem. Ut sit amet libero nec orci mattis fringilla. Praesent eu ipsum in sapien tincidunt molestie sed ut magna. Nam accumsan dui at orci rhoncus pharetra tincidunt elit ullamcorper. Sed ac mauris ipsum. Nullam imperdiet sapien id purus pretium id aliquam mi ullamcorper.</p>
    </div>
  </li>
  <li class="tab-header-and-content">
    <a href="javascript:void(0)" class="tab-link">Another Tab</a>
    <div class="tab-content">
      <p>Ut laoreet augue et neque pretium non sagittis nibh pulvinar. Etiam ornare tincidunt orci quis ultrices. Pellentesque ac sapien ac purus gravida ullamcorper. Duis rhoncus sodales lacus, vitae adipiscing tellus pharetra sed. Praesent bibendum lacus quis metus condimentum ac accumsan orci vulputate. Aenean fringilla massa vitae metus facilisis congue. Morbi placerat eros ac sapien semper pulvinar. Vestibulum facilisis, ligula a molestie venenatis, metus justo ullamcorper ipsum, congue aliquet dolor tortor eu neque. Sed imperdiet, nibh ut vestibulum tempor, nibh dui volutpat lacus, vel gravida magna justo sit amet quam. Quisque tincidunt ligula at nisl imperdiet sagittis. Morbi rutrum tempor arcu, non ultrices sem semper a. Aliquam quis sem mi.</p>
    </div>
  </li>
  <li class="tab-header-and-content">
    <a href="javascript:void(0)" class="tab-link">Third</a>
    <div class="tab-content">
      <p>Donec mattis mauris gravida metus laoreet non rutrum sem viverra. Aenean nibh libero, viverra vel vestibulum in, porttitor ut sapien. Phasellus tempor lorem id justo ornare tincidunt. Nulla faucibus, purus eu placerat fermentum, velit mi iaculis nunc, bibendum tincidunt ipsum justo eu mauris. Nulla facilisi. Vestibulum vel lectus ac purus tempus suscipit nec sit amet eros. Nullam fringilla, enim eu lobortis dapibus, quam magna tincidunt nibh, sit amet imperdiet dolor justo congue turpis.</p>    
    </div>
  </li>
  <li class="tab-header-and-content">
    <a href="javascript:void(0)" class="tab-link">Last Item</a>
    <div class="tab-content">
      <p>Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus dui urna.</p>
    </div>
  </li>
</ul>

<h2>Modal</h2>
<div class="modal">
  <label for="modal-1">
    <div class="modal-trigger">Click for Modal</div>
  </label>
  <input class="modal-state" id="modal-1" type="checkbox" />
  <div class="modal-fade-screen">
    <div class="modal-inner">
      <div class="modal-close" for="modal-1"></div>
      <h1>Modal Title</h1>
      <p class="modal-intro">Intro text lorem ipsum dolor sit ametm, quas, eaque facilis aliquid cupiditate tempora cumque ipsum accusantium illo modi commodi  minima.</p>
      <p class="modal-content">Body text lorem ipsum dolor ipsum dolor sit sit possimus amet, consectetur adipisicing elit. Itaque, placeat, explicabo, veniam quos aperiam molestias eriam molestias molestiae suscipit ipsum enim quasi sit possimus quod atque nobis voluptas earum odit accusamus quibusdam. Body text lorem ipsum dolor ipsum dolor sit sit possimus amet.</p>
    </div>
  </div>
</div>

<h2>Fade In</h2>
<div id="js-fadeInElement" class="fade-in-element">
  This box will fade in when it's <b>300px above the bottom</b> of the window.
  <br>
  And disappear as it goes <b>below the bottom</b> of the window.
  <br>
  If JavaScript is disabled, it will still show up.
</div>	
								
			</article>
			<?php endwhile; else: ?>
			<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
		<?php endif; ?>
	</div>
</section>

<?php get_footer(); ?>
