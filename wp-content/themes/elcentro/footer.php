</div>
<!-- begin the footer -->
<footer class="doc-foot">
	<p class="legal">&copy; <?php the_field('legal', 'options'); ?> <?php echo date("Y"); ?>.</p>
	<ul class="social">
		<li><a href="https://www.facebook.com/freedomcenterec" target="_blank"><i class="icon-facebook-square"></i></a></li>
	</ul>
</footer>
<?php wp_footer(); ?>
</body>
</html>