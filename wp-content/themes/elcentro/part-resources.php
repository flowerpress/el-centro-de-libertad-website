<?php
// $resources_settings = array(
//     'programs_title' => '',
//     'add_programs' => '',
//     'programs_description' => '',
//     'cta_link' => '',
//     'cta_title' => '',
//   );
//$my_array = $block['our_resources'];
//echo '<pre>'; print_r($my_array); echo '</pre>';

?>



<?php 
	
	/*
		The code in this file is an example off the code that you would use in your template to
		show the first X number of rows of a repeater.
		* Note there is additional code for this in the inc/theme-functions.php file.
		I'm sure there are more elegant ways to do the JavaScript, but what's here will work
	*/
	
	if (have_rows('our_resources')) {

		// set the id of the element to something unique
		// this id will be needed by JS to append more content
		$total = count(get_field('our_resources'));
		?>
  <div class="
    container
    module
    module--resources" 
  >
  <div class="container__content container__content--short ">
    <h2 class="green-header-uppercase"><?php the_field('our_resources_title'); ?></h2>
    <div class="module--resources__description"><?php the_field('our_resources_description'); ?></div>
    <div id="my-repeater-list-id" class="module--resources__wrap">
                               <?php
					$number = 10; // the number of rows to show
					$count = 0; // a counter

                    $items = array();
					while (have_rows('our_resources')) {
						the_row();

                        $items[] = (object)array(
                            'image' => get_sub_field('resource_image'),
                            'name' => get_sub_field('resource_name'),
                            'description' => get_sub_field('resource_description'),
                            'link' => get_sub_field('resource_link'),
                        );
                    }

                    // Sort alphabetically by name
                    usort($items, 'sort_by_name_alpha');

                    foreach ($items as $item) {
                                               ?>
            <div class="module--resources__resource">
              <?php
                if( !empty($item->image) ): ?>  
                <img src="<?php echo $item->image['url']; ?>" alt="<?php echo $item->image['alt']; ?>" />         
              <?php endif; ?>
              <?php if($item->name){ ?>
                <h3><?php echo $item->name; ?></h3>
              <?php } ?>
              <?php if($item->description){ ?>
                <div class="module--resources__resource__description">
                  <?php echo $item->description; ?>
                </div>
              <?php } ?>
              <?php if($item->link){ ?>
                <a class="cta-link" href="<?php echo $item->link; ?>">View Website</a>
              <?php } ?>
            </div>
						<?php 
						$count++;
						if ($count == $number) {
							// we've shown the number, break out of loop
							break;
						}
					} // end while have rows
				?>
    </div>
			<!-- 
				add a link to call the JS function to show more
				you will need to format this link using
				CSS if you want it to look like a button
				this button needs to be outside the container holding the
				items in the repeater field
			-->
			<a class="show_more module--resources__show-more" id="my-repeater-show-more-link" href="javascript: my_repeater_show_more();"<?php 
				if ($total < $count) {
					?> style="display: none;"<?php 
				}
				?>>Show More</a>
			<!-- 
				The JS that will do the AJAX request
			-->
  </div>
</div>

			<script type="text/javascript">
				var my_repeater_field_post_id = <?php echo $post->ID; ?>;
				var my_repeater_field_offset = <?php echo $number; ?>;
				var my_repeater_field_nonce = '<?php echo wp_create_nonce('our_resources_nonce'); ?>';
				var my_repeater_ajax_url = '<?php echo admin_url('admin-ajax.php'); ?>';
				var my_repeater_more = true;
				
				function my_repeater_show_more() {
					// make ajax request
					jQuery.post(
						my_repeater_ajax_url, {
							// this is the AJAX action we set up in PHP
							'action': 'my_repeater_show_more',
							'post_id': my_repeater_field_post_id,
							'offset': my_repeater_field_offset,
							'nonce': my_repeater_field_nonce
						},
						function (json) {
							// add content to container
							// this ID must match the containter 
							// you want to append content to
							jQuery('#my-repeater-list-id').append(json['content']);
							// update offset
							my_repeater_field_offset = json['offset'];
							// see if there is more, if not then hide the more link
							if (!json['more']) {
								// this ID must match the id of the show more link
								jQuery('#my-repeater-show-more-link').css('display', 'none');
							}
						},
						'json'
					);
				}
				
			</script>
		<?php 		
	} // end if have_rows
	
?>
