
/* Custom JS */


jQuery(document).ready(function($) {

  $('.js-hamburger').click(function() {
    $(this).toggleClass('is-active');
    $('.js-menu-main-nav-container').toggleClass('is-active');
  });
  
  $('.icon-search').click(function() {
  $(this).toggleClass('active');
  $('.header-search').slideToggle();
  $("html, body").animate({ scrollTop: 0 }, "fast");
  });
  
  $('.bxslider').bxSlider({
     auto: true,
     pager: false,
     controls: false,
     mode: 'fade',
     speed: 1500,
     touchEnabled: false
  });


  $('ul.js-donor-list').each(function(){
    if( $(this).find('li').length > 4){    
      $('li', this).eq(3).nextAll().hide().addClass('toggleable');
      $(this).append('<li class="show_more">Show More</li>');    
    }
    $(this).on('click','.show_more', toggleShow);
  });
  function toggleShow(){
    var opened = $(this).hasClass('show_less');  
    $(this).text(opened ? 'Show More' : 'Show Less').toggleClass('show_less', !opened);    
    $(this).siblings('li.toggleable').slideToggle();
  }

  // RSVP popup on homepage (commented out in case we need it back in future)

//   $(function()
//   {
//       if(sessionStorage.getItem('popState') != 'shown'){
//         $('.js-popup-con').fadeIn();
//         sessionStorage.setItem('popState','shown')
//       }
//       $('.js-message-close').click(function() {
//         $('.js-popup-con').fadeOut();
//       });
//   });

});