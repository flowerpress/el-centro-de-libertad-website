<?php
// $programs_settings = array(
//     'programs_title' => '',
//     'add_programs' => '',
//     'programs_description' => '',
//     'cta_link' => '',
//     'cta_title' => '',
//   );
//$my_array = $block['add_programs'];
//echo '<pre>'; print_r($my_array); echo '</pre>';

?>


<?php
if( have_rows('donor_block') ): ?>
  <div class="
    container 
    module
    module--donors" 
  >
    <div class="module--donors__wrap">
      <?php while ( have_rows('donor_block') ) : the_row(); ?>
        <div class="module--donors__block ">
         <h2 class="green-header-uppercase"><?php the_sub_field('donor_type'); ?></h2>
          <?php
          $donors = get_sub_field('donor');
          if($donors): ?>
            <ul class="js-donor-list">
<?php
      $items = array();
      foreach($donors as $donor) {
        $items[] = (object)array(
          'name' => $donor['donor_name'],
          'link' => $donor['donor_link'],
        );
      }
      // Sort alphabetically by name
      usort($items, 'sort_by_name_alpha');

      foreach($items as $donor) {
             if($donor->link):
             	echo '<li><a href="'. $donor->link .'">' . $donor->name . '</a></li>';
             else:
               echo '<li>' . $donor->name . '</li>';
             endif;
             ?>              
<?php
      }
?>
            </ul>
            <!-- <a href="#" class="cta-link js-load-more">Show more</a> -->
          <?php
            else:
            // no rows
            endif;
          ?>
        </div>
                   
       <?php endwhile; ?>
    </div>
  </div>
<?php
  else:
  // no rows
  endif;

  
?>
<script>
	jQuery(document).ready(function($) {
		
		var showMoreStringPHP = "<?php _e('Show More', 'el_centro_theme'); ?>";
		var showLessStringPHP = "<?php _e('Show Less', 'el_centro_theme'); ?>";
		
	  $('ul.js-donor-list').each(function(){
		if( $(this).find('li').length > 4){    
		  $('li', this).eq(3).nextAll().hide().addClass('toggleable');
		  $(this).append('<li class="show_more">'+ showMoreStringPHP +'</li>');    
		}
		$(this).on('click','.show_more', toggleShow);
	  });
	  function toggleShow(){
		var opened = $(this).hasClass('show_less');  
		$(this).text(opened ? showMoreStringPHP : showLessStringPHP).toggleClass('show_less', !opened);    
		$(this).siblings('li.toggleable').slideToggle();
	  }	
});
</script>