<?php
// $contact_settings = array(
//     'contact_title' => '',
//     'contact_description' => '',
//   );
//$my_array = $contact_settings;
//echo '<pre>'; print_r($my_array); echo '</pre>';

?>

<div id="contactCTASection" class="
  container 
  container--margin-inner
  module
  module--contact
  contactCTASection" 
>
  <div class="container__content container__content--medium">
    <h2 class="module--contact__title"><?php the_field('contact_cta_title', 'options'); ?></h2>
    <div class="module--contact__description"><?php the_field('contact_cta_description', 'options'); ?></div>
  </div>
  
</div>
