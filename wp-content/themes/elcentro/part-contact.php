<?php
// $contact_settings = array(
//     'contact_title' => '',
//     'contact_description' => '',
//   );
//$my_array = $contact_settings;
//echo '<pre>'; print_r($my_array); echo '</pre>';

?>

<div id="contactSection" class="
  container 
  container--bgr-blue
  container--margin-inner
  module
  module--contact" 
>
  <div class="container__content container__content--medium">
    <h2 class="module--contact__title h-dash-style"><?php the_field('contact_title', 'options'); ?></h2>
    <div class="module--contact__description"><?php the_field('contact_description', 'options'); ?></div>
    <div class="module--contact__form">     
      <?php
      if(ICL_LANGUAGE_CODE=='en'){
        echo do_shortcode( '[contact-form-7 id="166" title="Contact Footer"]' );
      }
      if(ICL_LANGUAGE_CODE=='es'){
        echo do_shortcode( '[contact-form-7 id="99" title="Contact Footer Spanish"]' );
      }
      ?>
    </div>
  </div>
  
</div>
