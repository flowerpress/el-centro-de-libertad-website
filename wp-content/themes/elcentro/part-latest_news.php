<?php
// $featured_posts_settings = array(
//     'latest_news__title' => '',
//     'featured_posts' => '',
//   );
//$my_array = $featured_posts_settings;
//echo '<pre>'; print_r($my_array); echo '</pre>';
$posts = $featured_posts_settings['featured_posts'];
?>

<div class="
  container
  container--bgr-yellow
  container--margin-inner
  module
  module--feature_posts"
>
  <div class="container__content">
    <h2 class="module--feature_posts__title h-dash-style"><?php echo $featured_posts_settings['latest_news__title']; ?></h2>

    <?php
    // check if we have posts
    if($posts):
      // loop through the rows of data
      $first=0;
      $count=0;
      if (sizeof($posts) <= 1): // Test idf only one post for diff styling
      
        foreach ($posts as $post) :
          $post_object = $post['featured_post'];
        if( $post_object ):
          $post = $post_object;
        setup_postdata( $post );
        $title = get_the_title();
        $link = get_the_permalink();
        $author = get_the_author();
        $categories = get_the_category();
        $date = get_the_time('m.d.Y');
        $excerpt = wp_trim_words(get_the_excerpt(), 46);
        ?>
        <div class="module--feature_posts__content module--feature_posts__content--singular">
          <h4><a href="<?php echo $link; ?>"><?php echo $title; ?></a></h4>
          <p class="post_author">Posted by: <strong><?php echo $author; ?></strong> on: <strong><?php echo $date; ?></strong></p>
          <?php if ($first == 0) : echo '<p class="module--feature_posts__excerpt">'.$excerpt.'</p>'; endif; ?>
        </div>
        <?php wp_reset_postdata();
        endif;
        $first++;
        endforeach;
      
      else :
      
        foreach ($posts as $post) :
          $post_object = $post['featured_post'];
        if( $post_object ):
          $post = $post_object;
        setup_postdata( $post );
        $title = get_the_title();
        $link = get_the_permalink();
        $author = get_the_author();
        $categories = get_the_category();
        $date = get_the_time('m.d.Y');
        $excerpt = wp_trim_words(get_the_excerpt(), 46);
        ?>
        <div class="module--feature_posts__content<?php if ($first == 0) : echo ' module--feature_posts__content--first'; endif; ?>">
          <h4><a href="<?php echo $link; ?>"><?php echo $title; ?></a></h4>
          <p class="post_author">Posted by: <strong><?php echo $author; ?></strong> on: <strong><?php echo $date; ?></strong></p>
          <?php if ($first == 0) : echo '<p class="module--feature_posts__excerpt">'.$excerpt.'</p>'; endif; ?>
        </div>
        <?php wp_reset_postdata();
        endif;
        $first++;
        endforeach;
        
      endif;
    
    else :
      // no rows found
    endif;
  ?>
  </div>
  <p class="module--feature_posts__cta"><a class="cta-button" href="<?php echo $block['cta_link'] ?>"><?php echo $block['cta_title'] ?></a></p>
</div>