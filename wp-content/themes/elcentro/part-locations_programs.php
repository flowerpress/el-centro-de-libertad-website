<?php
//$my_array = MY ARRAY;
//echo '<pre>'; print_r($my_array); echo '</pre>';
$colorBG =	get_field_object('choose_background_color'); 
?>

<div class="
  container
  container--center
  module
  module--locations_programs
  container--bgr-<?php echo $colorBG['value']; ?>"
	 >
<?php

// check if the repeater field has rows of data
$ai_title = __('Adult Intervention', 'el_centro_theme', 'program_title');
$yi_title = __('Youth Intervention', 'el_centro_theme', 'program_title');
$sp_title = __('Specialty Programs', 'el_centro_theme', 'program_title');
if( $locations_programs['add_programs'] ):
  // loop through the rows of data
  foreach ( $locations_programs['add_programs'] as $program_key => $program ) :
    $programSlug = $program['choose_program'];
  $programDescription = $program['program_description'];
	$programHeadline = $program['program_title'];
	

$programTitle = '';
switch ($programSlug) {
case 'adult':
  $programTitle = $ai_title;
  break;
case 'youth':
  $programTitle = $yi_title;
  break;
case 'specialty':
  $programTitle = $sp_title;
  break;
default:
  echo "No title found";
};
?>
	<?php $colorBG = get_field_object('choose_background_color'); ?>
	
   		<ul class="programs_list programs_list--locations_programs container--bgr-<?php echo $colorBG['value']; ?>">
     		<li class="">
     		  <?php
     		  if($program['program_link'])
     		  { ?>	  	
     		  	<a href="<?php echo $program['program_link'] ?>">
       		  	<span class="program_icon program_icon--<?php echo $programSlug ?> light-icon"></span>
       		  	<h2 class="green-header-uppercase"><?php echo $programHeadline ?></h2>
       		 </a>
     		  <?php } else { ?>
       		  <span class="program_icon program_icon--<?php echo $programSlug ?> light-icon"></span>
       		  <h2 class="green-header-uppercase"><?php echo $programHeadline ?></h2>
     		  <?php }
     		  
     		  ?>
 					<p><?php echo $programDescription; ?></p>
     		</li>
   		</ul>
    <?php endforeach;
else :

  // no rows found

  endif;

?>
</div>