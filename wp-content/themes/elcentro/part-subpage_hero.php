<?php 
$hero_mobile_image = get_field('hero_image_mobile'); 
$hero_desk_image = get_field('hero_image');

  if( !empty($hero_mobile_image) ): ?>
    <!--<style>
      .subpage_hero {
        background-image:url('<?php echo $hero_mobile_image['url']; ?>');
      }
      @media (min-width: 750px) {
        .subpage_hero {
          background-image:url('<?php echo $hero_desk_image['url']; ?>');
        }
      }
    </style>-->

<div class="subpage_hero_wrapper">
	<img src="<?php echo $hero_desk_image['url']; ?>" class="hero_image" />
	
	  <?php 
  
  else: ?>
  
    <style>
      .subpage_hero {
        background-image:url('<?php echo $hero_desk_image['url']; ?>');
      }
    </style>  
  
  <?php 
  endif; ?>
 
<div class="
  subpage_hero
  <?php if(empty($hero_desk_image)){ echo'subpage_hero--no-hero';}?>
  <?php if(get_field('hero_title')){ echo'subpage_hero--has-title';}?>"
>
 <?php
   if(get_field('hero_title'))
   {
   	echo '<h1 class="subpage_hero__title">' . get_field('hero_title') . '</h1>';
   }
 ?>
</div>
</div>



