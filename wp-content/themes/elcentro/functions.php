<?php
/**
 * @package WordPress
 * @subpackage Bitclone Boilerplate
 */


//set the include path
$inc_path = TEMPLATEPATH . '/inc/';

// General theme options.
require_once ($inc_path . 'theme-options.php');
// Theme widgeted areas.
require_once ($inc_path . 'theme-widgets.php');
// Theme Specific functions.
require_once ($inc_path . 'theme-functions.php');
// Theme Specific CPTs.
require_once ($inc_path . 'theme-functions-custom-post-types.php');

add_filter('run_wptexturize', '__return_false');

function modify_read_more_link() {
 return '<a class="more-link" href="' . get_permalink() . '">' . get_field('read_more_button_text', 'options') . '</a>';
}
add_filter( 'the_content_more_link', 'modify_read_more_link' );

add_theme_support( 'post-thumbnails' );

?>