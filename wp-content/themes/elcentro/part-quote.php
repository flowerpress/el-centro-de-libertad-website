<?php
// $quote_settings = array(
//     'quote_background' => '',
//     'quote_text' => '',
//     'quote_cite' => '',
//   );
// $my_array = $quote_settings;
// echo '<pre>'; print_r($my_array); echo '</pre>';
$quoteBG = get_field_object('quote_background_color');
?>
<div class="
  container 
  container--center
  container--margin-inner
  module
  module--quote
  container--bgr-<?php echo $quoteBG['value']; ?>"
>
  <div class="container__content container__content--short">
    <blockquote>
      <?php echo $quote_settings['quote_text'] ?>
      <cite><?php echo $quote_settings['quote_cite'] ?></cite>
    </blockquote>
  </div>
  
</div>
