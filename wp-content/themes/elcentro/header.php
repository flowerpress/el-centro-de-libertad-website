<!doctype html>
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<title>
<?php
    /*
     * Print the <title> tag based on what is being viewed.
     */
    global $page, $paged;

    wp_title( '|', true, 'right' );

?>
</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="author" content="Flowerpress Creative Studio" />
<meta name="copyright" content="(c) Copyright 2017." />

<!-- For IE 9 and below. ICO should be 32x32 pixels in size -->
<!--[if IE]><link rel="shortcut icon" href="/favicon.ico"><![endif]-->
<!-- Touch Icons - iOS and Android 2.1+ 180x180 pixels in size. --> 
<link rel="apple-touch-icon-precomposed" href="/apple-touch-icon-precomposed.png">
<!-- Firefox, Chrome, Safari, IE 11+ and Opera. -->
<link rel="icon" href="/favicon.png">
	
<link href="https://fonts.googleapis.com/css2?family=PT+Sans:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- Wrapper for mobile slide nav -->

    <!-- HEADER -->
    <header class="doc-head">
    	<div class="header-top">
      	 <?php // header widget for language switcher
        	  if ( ! dynamic_sidebar( 'header-widget' ) ) :
            endif;
          ?>
        	<?php if(is_front_page() ) {    
        			if(get_field('main_logo','options')) { ?>
        			<img class="logo-header" alt="El Centro de Libertad" src="<?php the_field('main_logo','options'); ?>">
        		<?php } else { ?>
        			<img class="logo-header" alt="El Centro de Libertad" src="<?php bloginfo('template_directory'); ?>/images/ElCentroLogo.png">
        		<?php }
      
        		} else {
        	
        			if(get_field('main_logo','options')) { ?>
        			<a href="<?php bloginfo('url'); ?>"><img class="logo-header" alt="El Centro de Libertad" src="<?php the_field('main_logo','options'); ?>"></a>
        		<?php } else { ?>
        			<a href="<?php bloginfo('url'); ?>"><img class="logo-header" alt="El Centro de Libertad" src="<?php bloginfo('template_directory'); ?>/images/ElCentroLogo.png"></a>
        		<?php }
        		
        		}?>
    	</div>
    	<div class="main_menu">
        <button class="hamburger hamburger--slider js-hamburger" type="button">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>
      	<?php wp_nav_menu( array('menu' => 'Main Nav', 'container' => 'nav', 'menu_class' => 'js-menu-main-nav-container' )); echo "\n"; ?>
    	</div>
    </header>
    <!-- End Header -->
	<div class="doc-wrap">