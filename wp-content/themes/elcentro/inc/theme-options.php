<?php

//Disable gutenberg blocks
add_filter('use_block_editor_for_post', '__return_false');

// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );

// Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );

// This theme uses custom menus
	add_theme_support( 'menus' );

// Add a favicon for your admin
function admin_favicon() {
	echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_bloginfo('template_directory').'/images/ico/favicon.ico" />';
}
add_action('admin_head', 'admin_favicon');


// Load up a google Fonts
function load_fonts() {
            wp_register_style('googleFonts', 'https://fonts.googleapis.com/css?family=Lato:400,400i,700|Satisfy|Parisienne');
            wp_enqueue_style( 'googleFonts');
        }
    
    add_action('wp_print_styles', 'load_fonts');

// jQuery from Google http://css-tricks.com/snippets/wordpress/include-jquery-in-wordpress-theme/
if (!is_admin()) add_action("wp_enqueue_scripts", "custom_jquery_enqueue", 11);
function custom_jquery_enqueue() {
   wp_enqueue_script( 'site-js-minified', get_template_directory_uri() . '/js/app.min.js' );
}

// Add ie conditional html5 shim to header
function add_ie_html5_shim () {
    echo '<!--[if lt IE 9]>';
    echo '<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>';
    echo '<![endif]-->';
}
add_action('wp_head', 'add_ie_html5_shim');


// Add page slug to body class...helpful!

function add_slug_body_class( $classes ) {
global $post;
if ( isset( $post ) ) {
$classes[] = $post->post_name;
}
return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

/* We want to add a few custom styles to our Visual editor - http://alisothegeek.com/2011/05/tinymce-styles-dropdown-wordpress-visual-editor/ */

add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );

function my_mce_buttons_2( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}

add_filter( 'tiny_mce_before_init', 'my_mce_before_init' );

function my_mce_before_init( $settings ) {

    $style_formats = array(
		array(
        	'title' => 'CTA Button',
        	'selector' => 'a',
          'classes' => 'cta-button'
        ),
		array(
        	'title' => 'Green Header Uppercase',
        	'selector' => 'H2',
          'classes' => 'green-header-uppercase'
        ),
		array(
        	'title' => 'Bullet List',
        	'selector' => 'ul',
          'classes' => 'bullet-list'
      ),
    array(
        	'title' => 'No Margin Paragraph',
        	'selector' => 'p',
          'classes' => 'no-margin'
      )
    );

    $settings['style_formats'] = json_encode( $style_formats );

    return $settings;

}

/* Add ACF Options Page */

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}

// Move Yoast to bottom
function yoasttobottom() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');
?>