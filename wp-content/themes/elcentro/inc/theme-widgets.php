<?php
/**
 * Register widgetized areas.
 */
function bitclonetheme_widgets_init() {
	// Area 1,  Secondary Widget Area.
	register_sidebar( array(
		'name' => __( 'Primary', 'Bitclone' ),
		'id' => 'primary-widget',
		'description' => __( 'Primary', 'Bitclone' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Header', 'Bitclone' ),
		'id' => 'header-widget',
		'class' => 'language-switcher',
		'description' => __( 'Header', 'Bitclone' ),
		'before_widget' => '<div id="%1$s" class="di-widget %2$s">',
    'after_widget'  => '</div>',
		'before_title' => '',
		'after_title' => '',
	) );
}
/** Register sidebars by running bitclonetheme_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'bitclonetheme_widgets_init' );
?>