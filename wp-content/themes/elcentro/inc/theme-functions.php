<?php
/*
 * Site Specific Functions for content etc.
 */
	
/*
  	this is an example of how to create a repeater that will show a specific number of rows
  	and add a link (button) to display more rows
  	
  	this code would generally be added to your functions.php file
  	
  	Note: This will only work for a single repeater field. You will need to add different functions
  	with different names to use this on multiple repeater field
  	
  	The code in this file should go into your function.php file
  	or be included by your functions.php file
  	
  	Please note that this example uses jQuery to perform the AJAX request
  	You must enqueue jQuery using a wp_enqueue_scripts action
*/

    /* sort_by_name_alpha() */
    function sort_by_name_alpha($a, $b) {
        $key = 'name';

        if($a->{$key} < $b->{$key}){
            return -1;
        }else if($a->{$key} > $b->{$key}){
            return 1;
        }
        return 0;
    }

	// add action for logged in users
	add_action('wp_ajax_my_repeater_show_more', 'my_repeater_show_more');
	// add action for non logged in users
	add_action('wp_ajax_nopriv_my_repeater_show_more', 'my_repeater_show_more');
	
	function my_repeater_show_more() {
		// validate the nonce
		if (!isset($_POST['nonce']) || !wp_verify_nonce($_POST['nonce'], 'our_resources_nonce')) {
			exit;
		}
		// make sure we have the other values
		if (!isset($_POST['post_id']) || !isset($_POST['offset'])) {
			return;
		}
		$show = 4; // how many more to show
		$start = $_POST['offset'];
		$end = $start+$show;
		$post_id = $_POST['post_id'];
		// use an object buffer to capture the html output
		// alternately you could create a varaible like $html
		// and add the content to this string, but I find
		// object buffers make the code easier to work with
        $items = array();
		ob_start();
		if (have_rows('our_resources', $post_id)) {
			$total = count(get_field('our_resources', $post_id));
			$count = 0;

			while (have_rows('our_resources', $post_id)) {
				the_row();

                $items[] = (object)array(
                    'image' => get_sub_field('resource_image'),
                    'name' => get_sub_field('resource_name'),
                    'description' => get_sub_field('resource_description'),
                    'link' => get_sub_field('resource_link'),
                );
            }

            // Sort alphabetically by name
            usort($items, 'sort_by_name_alpha');

            foreach ($items as $item) {
                if ($count < $start) {
                    // we have not gotten to where
                    // we need to start showing
                    // increment count and continue
                    $count++;
                    continue;
                }
?>
          <div class="module--resources__resource">
          <?php
            if( !empty($item->image) ): ?>  
            <img src="<?php echo $item->image['url']; ?>" alt="<?php echo $item->image['alt']; ?>" />         
          <?php endif; ?>
          <?php if($item->name){ ?>
            <h3><?php echo $item->name; ?></h3>
          <?php } ?>
          <?php if($item->description){ ?>
            <div class="module--resources__resource__description">
              <?php echo $item->description; ?>
            </div>
          <?php } ?>
          <?php if($item->link){ ?>
            <a class="cta-link" href="<?php echo $item->link; ?>">View Website</a>
          <?php } ?>
          </div>     
        <?php
                $count++;
                if ($count == $end) {
                    // we've shown the number, break out of loop
                    break;
                }
			} // end while have rows
		} // end if have rows
		$content = ob_get_clean();
		// check to see if we've shown the last item
		$more = false;
		if ($total > $count) {
			$more = true;
		}
		// output our 3 values as a json encoded array
		echo json_encode(array('content' => $content, 'more' => $more, 'offset' => $end));
		exit;
	} // end function my_repeater_show_more
	

	add_filter('rest_url', function($url) {
    $result = preg_match('/(\?.+)\//U', $url, $matches);
    if (!$result || !isset($matches[0]) || !isset($matches[1])) {
        return $url;
    }
    $url = str_replace($matches[0], '', $url);

    return $url . $matches[1];
});
?>