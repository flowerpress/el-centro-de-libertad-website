<?php
// Register Custom Post Type
/*Custom Post type start*/

function ec_post_type_jobs() {

  $supports = array(
  'title', // post title
  'editor', // post content
  'author', // post author
  'custom-fields', // custom fields
  'revisions', // post revisions
  'post-formats', // post formats
  );
  
  $labels = array(
  'name' => _x('Jobs', 'plural'),
  'singular_name' => _x('Job', 'singular'),
  'menu_name' => _x('Jobs', 'admin menu'),
  'name_admin_bar' => _x('Jobs', 'admin bar'),
  'add_new' => _x('Add New Job', 'add new job'),
  'add_new_item' => __('Add New Jobs'),
  'new_item' => __('New Job'),
  'edit_item' => __('Edit Job'),
  'view_item' => __('View Jobs'),
  'all_items' => __('All Jobs'),
  'search_items' => __('Search jobs'),
  'not_found' => __('No Jobs found.'),
  );
  
  $args = array(
  'supports' => $supports,
  'labels' => $labels,
  'public' => true,
  'capability_type' => 'page',	  
  'query_var' => true,
  'rewrite' => array('slug' => 'jobs'),
  'has_archive' => true,
  'hierarchical' => false,
  );
  register_post_type('jobs', $args);
  }
  add_action('init', 'ec_post_type_jobs');
  
  /*Custom Post type end*/

?>