<?php get_header();


$hero_mobile_image = get_field('hero_image_mobile', get_option('page_for_posts')); 
$hero_desk_image = get_field('hero_image', get_option('page_for_posts'));

  if( !empty($hero_mobile_image) ): ?>
    <style>
      .subpage_hero {
        background-image:url('<?php echo $hero_mobile_image['url']; ?>');
      }
      @media (min-width: 750px) {
        .subpage_hero {
          background-image:url('<?php echo $hero_desk_image['url']; ?>');
        }
      }
    </style>
  <?php 
  
  else: ?>
  
    <style>
      .subpage_hero {
        background-image:url('<?php echo $hero_desk_image['url']; ?>');
      }
    </style>  
  
  <?php 
  endif; ?>

<div class="
  subpage_hero
  subpage_hero--has<?php if(get_field('hero_title', get_option('page_for_posts'))){ echo'-title';}?>"
>
 <?php
   if(get_field('hero_title', get_option('page_for_posts')))
   {
   	echo '<h1 class="subpage_hero__title">' . get_field('hero_title', get_option('page_for_posts')) . '</h1>';
   }
 ?>
</div>


<!-- begin content -->
<section class="container">
	<div class="container__content container__content--short">
		 <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		<article role="article" <?php post_class('editable_content job-opening') ?> id="post-<?php the_ID(); ?>">
		
		    <h1 class="job-title"><?php echo __('El Centro de Libertad/The Freedom Center', 'el_centro_theme'); ?><br><?php the_title(); ?></h1>
        <?php if(get_field('summary')) { ?>
            <?php the_field('summary'); ?>
        <?php } ?>

		</article>
		<?php endwhile; else: ?>
		<p>
		  <?php _e('Sorry, no posts matched your criteria.'); ?>
		</p>
		<?php endif; ?>
	
	</div>

</section>
<!-- end content -->


<?php
	include ( 'part-contact-cta.php' );
?>


<?php get_footer(); ?>