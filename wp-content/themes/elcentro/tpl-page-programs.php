<?php
/**
 * Template Name: Programs Page
 *
 * A custom page template.
 * @package WordPress
 * @subpackage Bitclone Boilerplate
 */

get_header();

if (have_posts()) : while (have_posts()) : the_post(); ?>
 
<!-- Hero -->

<?php
  $hero_desk_image = get_field('hero_image');
  if( !empty($hero_desk_image) ):
	  include ( 'part-subpage_hero.php' );
	endif;

$locationsBG = get_field_object('locations_background_color');
$clientfeeBG = get_field_object('client_fee_background_color');
?>

<!-- begin content -->
<?php if(get_field('intro_text')) { ?>
<?php $introBG = get_field_object('intro_background_color'); ?>
<div class="container intro-container container--bgr-<?php echo $introBG['value']; ?>">
  <div class="container__content container__content--short">  
    <?php the_field('intro_text'); ?>
  </div>
</div>
<?php } ?>

<div class="locations-wrapper container--bgr-<?php echo $locationsBG['value']; ?>">
<?php
if(get_field('locations')) {
	include ( 'part-locations.php' );
}
?>
</div>

<div class="client-fees-wrapper <?php echo $clientfeeBG['value']; ?>">
	<?php include ( 'part-client-fees.php' );?>
</div>


<?php // Check for Gift or Contact global modules
  

if(get_field('add_contact_block')) {
	include ( 'part-contact.php' );
}
if(get_field('add_contact_cta_block')) {
	include ( 'part-contact-cta.php' );
}
if(get_field('add_gift_block')) {
	$make_gift = array(
  	'background_color' => 'yellow',
	);
	include ( 'part-gift.php' );
}

?>

<?php endwhile; else: ?>
<?php endif; ?>

<?php get_footer(); ?>