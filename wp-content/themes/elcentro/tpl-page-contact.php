<?php
/**
 * Template Name: Contact Page
 *
 * A custom page template.
 * @package WordPress
 * @subpackage FPCS
 */

get_header();
 
if (have_posts()) : while (have_posts()) : the_post();
?>

<!-- Hero -->

<?php
  $hero_desk_image = get_field('hero_image');
  if( !empty($hero_desk_image) ):
	  include ( 'part-subpage_hero.php' );
	endif;
?>
<?php 
	$introBG = get_field_object('intro_background_color'); 
?>


<!-- begin content -->
<?php if(get_field('intro_text')) { ?>
<div class="container container--margin-inner contact-intro-container container--bgr-<?php echo $introBG['value']; ?>">
  <div class="container__content container__content--short">  
    <?php the_field('intro_text'); ?>
  </div>
</div>
<?php } ?>


<?php include ( 'part-locations_details.php' ); ?>

<?php
if(get_field('add_contact_block')) {
	include ( 'part-contact.php' );
}
?>

<?php
if(get_field('add_contact_cta_block')) {
	include ( 'part-contact-cta.php' );
}
?>

<?php // Check for Gift or Contact global modules
  
if(get_field('add_gift_block')) {
	$make_gift = array(
  	'background_color' => 'yellow',
	);
	include ( 'part-gift.php' );
}

?>

<?php endwhile; else: ?>
<?php endif; ?>

<?php get_footer(); ?>