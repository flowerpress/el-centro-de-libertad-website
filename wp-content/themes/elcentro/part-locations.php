<?php
// $programs_settings = array(
//     'programs_title' => '',
//     'add_programs' => '',
//     'programs_description' => '',
//     'cta_link' => '',
//     'cta_title' => '',
//   );
//$my_array = $block['add_programs'];
//echo '<pre>'; print_r($my_array); echo '</pre>';

?>

<?php
if( have_rows('locations') ): ?>
  <div class="
    container 
    module
    module--locations
	container--bgr-<?php echo $locationsBG['value']; ?>" 
  >
  <div class="container__content">
    <h2 class="module--locations__title"><?php the_field('locations_title'); ?></h2>
    <div class="module--locations__wrap">
      <?php while ( have_rows('locations') ) : the_row(); ?>
         
       <div class="module--locations__location">
         <?php
           $image = get_sub_field('location_image');     
           if( !empty($image) ): ?>  
           <a href="<?php the_sub_field('location_link'); ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>         
         <?php endif; ?>
		   <h3 class="location-name"><?php the_sub_field('location_name'); ?></h3>
		   <p class="location-description"><?php the_sub_field('location_description'); ?></p>
         <a class="cta-button" href="<?php the_sub_field('location_link'); ?>"><?php the_sub_field('location_button_text'); ?></a>
       </div>
                   
       <?php endwhile; ?>
    </div>
  </div>
</div>
<?php
  else:
  // no rows
  endif;
?>
