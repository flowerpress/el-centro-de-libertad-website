<?php
// $contact_settings = array(
//     'client_fees_title' => '',
//     'client_fees_description' => '',
//   );
//$my_array = $client_fees_settings;
//echo '<pre>'; print_r($my_array); echo '</pre>';

?>

<div id="contactSection" class="
  container 
  container--bgr-<?php echo $clientfeeBG['value']; ?>
  container--margin-inner
  module
  module--contact" 
>
  <div class="container__content container__content--medium">
    <h2 class="module--contact__title"><?php the_field('client_fees_title', 'options'); ?></h2>
    <div class="module--contact__description"><?php the_field('client_fees_description', 'options'); ?></div>
		<div style="text-align: center;">
      <?php the_field('client_fees_donor_box_code', 'options'); ?>
		</div>
  </div>
  
</div>
