<?php
// $programs_settings = array(
//     'programs_title' => '',
//     'add_programs' => '',
//     'programs_description' => '',
//     'cta_link' => '',
//     'cta_title' => '',
//   );
//$my_array = $block['add_programs'];
//echo '<pre>'; print_r($my_array); echo '</pre>';

?>

<div class="
  container
  container--center
  container--margin-inner
  module
  module--programs"
	 style="background-image: url('<?php echo get_template_directory_uri();?>/images/programs-bg.jpg');">
  <div class="container__content container__content--short container_programs">
	  <div class="content-wrapper">
    <h2 class="module--programs__title"><?php echo $programs_settings['programs_title'] ?></h2>
    <?php echo $programs_settings['programs_description'] ?>
    
    <a class="cta-button" href="<?php echo $programs_settings['cta_link'] ?>"><?php echo $programs_settings['cta_title'] ?></a>
	  </div>
  </div>

</div>