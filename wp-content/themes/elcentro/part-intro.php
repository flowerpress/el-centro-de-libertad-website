<?php
// $intro_settings = array(
//     'intro_content' => '',
//   );
// $my_array = $block;
// echo '<pre>'; print_r($my_array); echo '</pre>';
$introBG = get_field_object('home_intro_background_color');
$awardImage = get_field('intro_award_image');
$slide_image = get_field('slide_image');
?>

<div class="container intro-container intro-home container--bgr-<?php echo $introBG['value']; ?>">
  <div class="container__content container__content--short editable_content">
    <?php echo $block['intro_content'] ?>
  </div>
  
</div>
