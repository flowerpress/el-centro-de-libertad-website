<?php get_header();?>
<?php
  $hero_desk_image = get_field('jobs_hero_image', 'options');
  if( !empty($hero_desk_image) ):
	  include ( 'part-careers_subpage_hero.php' );
	endif;
?>
 
<!-- begin content -->
<?php if(get_field('jobs_intro_text', 'options')) { ?>
<div class="container container--margin-inner intro-container container--bgr-<?php echo get_field('jobs_intro_background_color', 'options'); ?>">
  <div class="container__content container__content--660">  
    <?php echo get_field('jobs_intro_text', 'options'); ?>
  </div>
</div>
<?php } ?>


<!-- begin content -->
<section class="container container--bgr-<?php echo get_field('jobs_background_color', 'options'); ?>">
	<div class="container__content container__content--short">
  <h2><?php echo __('Career Openings', 'el_centro_theme'); ?></h2>

		 <?php
$loop = new WP_Query(
    array(
        'post_type' => 'jobs', // This is the name of your post type - change this as required,
        'posts_per_page' => 5 // This is the amount of posts per page you want to show
    )
);
while ( $loop->have_posts() ) : $loop->the_post();
// The content you want to loop goes in here:
?>
 
<article role="article" <?php post_class('editable_content') ?> id="post-<?php the_ID(); ?>">
				
		           <h4><?php the_title(); ?></h4>
				<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">
					<h6>View Listing ></h6>
				</a>
		</article>
 
<?php endwhile;
wp_reset_postdata();
?>
	
	</div>

</section>
<!-- end content -->


<?php
	$make_gift = array(
  	'background_color' => 'yellow',
	);
	include ( 'part-gift.php' );
?>

<?php get_footer(); ?>