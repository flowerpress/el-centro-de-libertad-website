<?php
/**
 * Template Name: About Page
 *
 * A custom page template.
 * @package WordPress
 * @subpackage FPCS
 */

get_header();
 
if (have_posts()) : while (have_posts()) : the_post();
?>

<!-- Hero -->

<?php
  $hero_desk_image = get_field('hero_image');
  if( !empty($hero_desk_image) ):
	  include ( 'part-subpage_hero.php' ); 
	endif;
?>
<?php
$introBG = get_field_object('intro_background_color');
$ourboardBG = get_field_object('our_board_background_color');
$ourstaffBG = get_field_object('our_staff_background_color');
$careersBG = get_field_object('careers_background_color');
?>


<!-- begin content -->
<?php if(get_field('intro_text')) { ?>
<div class="container container--margin-inner about-intro-container container--bgr-<?php echo $introBG['value']; ?>">
  <div class="container__content container__content--short">  
    <?php the_field('intro_text'); ?>
  </div>
</div>
<?php } ?>



<?php

  $blocks = get_field('quote_content');
  if ( is_array($blocks) ) :
    foreach ($blocks as $block_key => $block) :
  
      switch ($block['acf_fc_layout']) :
	case 'quote':
			  $quote_settings = array(
				'quote_text' => $block['quote_text'],
				'quote_cite' => $block['quote_cite'],
			  );
			  include ( 'part-quote.php' );
			  break;

 			default:

          break;
      endswitch;
    endforeach;
  endif; 
?>



<?php if(get_field('our_board')) { ?>
<div class="container about-our-board-container container--bgr-<?php echo $ourboardBG['value']; ?>">
  <div class="container__content container__content--short">  
    <?php the_field('our_board'); ?>
  </div>
</div>
<?php } ?>

<?php if(get_field('our_staff')) { ?>
<div class="container staff-items container--bgr-<?php echo $ourstaffBG['value']; ?>">
  <div class="container__content container__content--short">  
    <?php the_field('our_staff'); ?>
  </div>
</div>
<?php } ?>

<?php if(get_field('careers')) { ?>
<div class="container career-items container--bgr-<?php echo $careersBG['value']; ?>">
  <div class="container__content container__content--short">  
    <?php the_field('careers'); ?>
	  <a href="<?php the_field('cta_link') ?>" class="cta-button"><?php the_field('cta_title'); ?></a>
  </div>
</div>
<?php } ?>

<?php
if(get_field('add_contact_cta_block')) {
	include ( 'part-contact-cta.php' );
}
?>

<?php // Check for Gift or Contact global modules



if(get_field('add_gift_block')) {
	$make_gift = array(
  	'background_color' => 'yellow',
	);
	include ( 'part-gift.php' );
}

?>

<?php endwhile; else: ?>
<?php endif; ?>

<?php get_footer(); ?>