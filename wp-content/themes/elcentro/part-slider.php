<!-- BX Slider -->
<div class="img_slider">
  <ul class="bxslider">
  <?php
      $slide_text = get_field('slide_text');
      $slide_image = get_field('slide_image');
      //$slide_image_logo = get_field('slide_logo');
      ?>        
      		<li class="img_slider__slide">
      		  <img src="<?php echo $slide_image['url']; ?>" alt="<?php echo $slide_image['alt']; ?>" />
      		  <div class="img_slider__title">
        		  <?php if( $slide_text ): ?>
        		  <h1><?php echo $slide_text ?></h1>
        		  <?php endif; ?>
      		  </div>
      		</li>
  </ul>
</div>
