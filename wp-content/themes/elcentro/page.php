<?php
get_header();
 
if (have_posts()) : while (have_posts()) : the_post();
?>
 
<!-- Hero -->

<?php
  $hero_desk_image = get_field('hero_image');
  if( !empty($hero_desk_image) ):
	  include ( 'part-subpage_hero.php' );
	endif;
?>

<!-- begin content -->
<?php if(get_field('intro_text')) { ?>
<?php $introBG = get_field_object('intro_background_color'); ?>
<div class="container intro-container container--bgr-<?php echo $introBG['value']; ?>">
  <div class="container__content container__content--short">  
    <?php the_field('intro_text'); ?>
  </div>
</div>
<?php } ?>

<?php 
if( '' !== get_post()->post_content ) { ?>
<div class="container">
  <div class="container__content">  
    <?php the_content(); ?>
  </div>
</div>
<?php } ?>


<?php

// Client Fees Block Option
if(get_field('add_client_fees_block'))
{
	include ( 'part-client_fees.php' );
}
// Contact Block Option
if(get_field('add_contact_block'))
{
	include ( 'part-contact.php' );
}
// Contact CTA Block Option
if(get_field('add_contact_cta_block')) {
	include ( 'part-contact-cta.php' );
}
// Gift Block Option
if(get_field('add_gift_block'))
{
	$block = array(
  	'background_color' => 'yellow',
	);
	include ( 'part-gift.php' );
}

?>
<?php endwhile; else: ?>
<?php endif; ?>

<?php get_footer(); ?>