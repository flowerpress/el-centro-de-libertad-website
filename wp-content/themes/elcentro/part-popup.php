<div class="popup-con js-popup-con">
  <div class="popup-inner-con">
    <div class="save-date">
    <span class="save-date__spacer"></span>
      <span>Save the Date</span>
      <button class="js-message-close close-x"></button>
    </div>
    <div class="popup-header">
      <img class="logo-25" src="<?php bloginfo('template_directory'); ?>/images/25_logo.svg" />
    </div>
    <div class="popup-content">
      <span class="popup-content__invited">You are invited</span>
      <span class="popup-content__date">July 19, 2019</span>
      <span class="popup-content__time">3:00–6:00 PM</span>
      <span class="popup-content__address">500 Allerton Street  •  Redwood City, CA 94063</span>

      <span class="popup-content__features">Raffle • Silent Auction • Music • Hors d'oeuvres • Keynote</span>
      <p>We hope to see you there!</p>
      <a href="/contact#contactSection" class="cta-button js-message-close">RSVP</a>
      <p>Cannot attend?</p>
      <a class="popup-content__donate" href="/make-a-gift">Please consider making a donation</a>
    </div>
  </div>
</div>