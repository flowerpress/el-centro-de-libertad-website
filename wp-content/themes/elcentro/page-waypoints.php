<?php get_header(); ?>
<style>
.os-animation,
.staggered-animation{
  opacity: 0;
} 
.os-animation.animated,
.staggered-animation.animated{
    opacity: 1;
}
.stuck {
  position:fixed;
  top:0;
  z-index: 9999999;
  width: 100%;
}
.sticky {
	background: #ccc;
	z-index: 99999999;
}
.bottoms-up {
	background: blue;
	color: #fff;
	height: 200px;
}
</style>
<script>
jQuery(document).ready(function($) { // http://www.oxygenna.com/tutorials/scroll-animations-using-waypoints-js-animate-css

	function onScrollInit( items, trigger ) {
	  items.each( function() {
	    var osElement = $(this),
	        osAnimationClass = osElement.attr('data-os-animation'),
	        osAnimationDelay = osElement.attr('data-os-animation-delay');
	 
	    osElement.css({
	        '-webkit-animation-delay':  osAnimationDelay,
	        '-moz-animation-delay':     osAnimationDelay,
	        'animation-delay':          osAnimationDelay
	    });
	 
	    var osTrigger = ( trigger ) ? trigger : osElement;
	 
	    osTrigger.waypoint(function() {
	        osElement.addClass('animated').addClass(osAnimationClass);
	    },{
	        triggerOnce: true,
	        offset: '50%'
	    });
	  });
	}
	
	onScrollInit( $('.os-animation') );
	onScrollInit( $('.staggered-animation'), $('.staggered-animation-container') );
	
	var sticky = new Waypoint.Sticky({
	element: $('.sticky')[0]
	});
	$('.destroy-sticky').on('click', function() {
	  sticky.destroy() 
	});

	$('.bottoms-up')
	  .css('opacity', 0) // immediately hide element
	  .waypoint(function(direction) {
	    if (direction === 'down') {
	      $(this.element).animate({ opacity: 1 })
	    }
	    else {
	      $(this.element).animate({ opacity: 0 })
	    }
	  }, {
	    offset: 'bottom-in-view'
	  })

});
</script>

<!-- begin content -->
<section class="doc-content subpage full">
	<div>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<article id="page-content" role="article" <?php post_class() ?> id="Post-<?php the_ID(); ?>">
				<?php the_content(__('(more...)')); ?>										
			</article>
			<?php endwhile; else: ?>
			<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
		<?php endif; ?>
		<p class="sticky">Stick me to the top yo. I'm STICKY!</p>
		<div style="padding-top: 600px;">Spacer Div</div>
		<section class="staggered-animation-container">
		    <h1>This section contains staggered animations!</h1>
		    <p class="staggered-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.5s">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
		    <p class="staggered-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.8s">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
		    <p class="staggered-animation" data-os-animation="fadeInUp" data-os-animation-delay="1.1s">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
		</section>
	
		<section class="os-animation" data-os-animation="swing" data-os-animation-delay="0s">
		    <h1>This section will swing</h1>
		    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
		</section>
		
		<p><button class="destroy-sticky">Destroy Sticky!</button></p>
		
		<p class="bottoms-up">Topper Bottoms!</p>
		
		<p style="margin-top: 100px;" class="bottoms-up">Topper Bottoms 2!</p>
		
	</div>
</section>

<?php get_footer(); ?>
