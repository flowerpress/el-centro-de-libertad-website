<?php
/**
 * @package WordPress
 * @subpackage Bitclone Boilerplate
 */
?>
<!-- begin sidebar -->
<ul>
	<?php if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>
	<?php endif; // end primary widget area ?>
</ul>

<!-- end sidebar -->
