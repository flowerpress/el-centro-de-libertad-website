<?php
/**
 * Template Name: Programs Single Page
 *
 * A custom page template.
 * @package WordPress
 * @subpackage Bitclone Boilerplate
 */

get_header();

if (have_posts()) : while (have_posts()) : the_post(); 
?>
 
<!-- Hero -->

<?php
  
  $hero_desk_image = get_field('hero_image');
  if( !empty($hero_desk_image) ):
	  include ( 'part-subpage_hero.php' );
	endif;
?>

<!-- begin content -->
<?php if(get_field('intro_text')) { ?>
<?php $introBG = get_field_object('intro_background_color'); ?>
<div class="container container--margin-inner container--bgr-<?php echo $introBG['value']; ?>">
  <div class="container__content container__content--short">
    
    <!-- Page Icon -->
    <?php if(get_field('page_icon')) { ?> 
        <p class="text-center"><span class="program_icon program_icon--<?php the_field('page_icon'); ?> light-icon"></span></p>
    <?php } ?>
    <?php the_field('intro_text'); ?>
  </div>
</div>
<?php } ?>



<?php
if( have_rows('content_rows') ): ?>
<div class="content_rows">
   <?php while ( have_rows('content_rows') ) : the_row(); ?>
	<?php $colorBG = the_sub_field('choose_background_color'); ?>
<div class="container container--divider container--bgr-<?php echo $colorBG['value']; ?>">
  <div class="container__content container__content--large editable_content">       
        <h2 class="green-header-uppercase"><?php the_sub_field('content_row_title'); ?></h2>
        <?php
        if( have_rows('content_row_images') ): ?>
        <div class="content_rows__images"> 
           <?php while ( have_rows('content_row_images') ) : the_row(); ?>             
                <?php
                  $image = get_sub_field('content_row_image');     
                  if( !empty($image) ): ?>  
                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />         
                <?php endif; ?>                
            <?php endwhile; ?>
        </div>
        <?php
          else:
          // no rows
          endif;
        ?>
        <?php the_sub_field('content_row_text'); ?>
  </div>
</div>
    <?php endwhile; ?>
</div>
<?php
  else:
  // no rows
  endif;
?>

<?php  // Page Parent Link
if ( $post->post_parent ) { ?>
<div class="container">
  <div class="container__content container__content--large"> 
     <p class="text-center">
    <a class="cta-link cta-link--back" href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo __('Back to','el_centro_theme'); ?> <?php echo get_the_title( $post->post_parent ); ?> <?php echo __('Programs','el_centro_theme'); ?></a></p>
  </div>
</div>
<?php } ?>


<?php // Check for Gift or Contact global modules
  
if(get_field('add_contact_block')) {
	include ( 'part-contact.php' );
}


if(get_field('add_contact_cta_block')) {
	include ( 'part-contact-cta.php' );
}


if(get_field('add_gift_block')) {
	$make_gift = array(
  	'background_color' => 'yellow',
	);
	include ( 'part-gift.php' );
}

?>

<?php endwhile; else: ?>
<?php endif; ?>

<?php get_footer(); ?>