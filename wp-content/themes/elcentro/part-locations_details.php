<?php
//$my_array = MY ARRAY;
//echo '<pre>'; print_r($my_array); echo '</pre>';
?>

<?php
if( have_rows('locations_details') ): ?>
  <div class="
    container 
    module
    module--locations_details
	container--bgr-<?php echo get_field('locations_background_color')?>" 
  >
  <div class="container__content container__content--short">
    <div class="module--locations_details__wrap">
      <?php while ( have_rows('locations_details') ) : the_row(); ?>

         <ul class="module--locations_details__location">
         	  <li><h2 class="location-title-header"><?php the_sub_field('location_name'); ?></h2></li>
         	  <li><?php the_sub_field('location_address'); ?></li>
          	<li><?php the_sub_field('location_phone'); ?></li>
            <li><?php the_sub_field('location_hours'); ?></li>
         </ul>
                   
       <?php endwhile; ?>
    </div>
  </div>
</div>
<?php
  else:
  // no rows
  endif;
?>
