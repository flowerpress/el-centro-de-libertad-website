<?php
/**
 * Template Name: Locations Programs Page
 *
 * A custom page template.
 * @package WordPress
 * @subpackage FPCS
 */

get_header();
 
if (have_posts()) : while (have_posts()) : the_post();
?>

<!-- Hero -->

<?php
  //$introBG = get_field_object('intro_background_color');
  $hero_desk_image = get_field('hero_image');
  if( !empty($hero_desk_image) ):
	  include ( 'part-subpage_hero.php' );
	endif;
?>

<!-- begin content -->
<?php if(get_field('intro_text')) { ?>
<?php $introBG = get_field_object('intro_background_color'); ?>
<div class="container container--bgr-<?php echo $introBG['value']; ?>">
  <div class="container__content container__content--short">  
    <?php the_field('intro_text'); ?>
  </div>
</div>
<?php } ?>


<?php
if(get_field('add_programs')) {
  $locations_programs = array(
    "add_programs" => get_field('add_programs'),
  );
	include ( 'part-locations_programs.php' );
}
?>

<?php  // Page Parent Link
if ( $post->post_parent ) { ?>
<div class="container">
  <div class="container__content container__content--large"> 
     <p class="text-center">
    <a class="cta-link cta-link--back" href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo __('Back to','el_centro_theme'); ?> <?php echo get_the_title( $post->post_parent ); ?></a></p>
  </div>
</div>
<?php } ?>

<?php // Check for Gift or Contact global modules
  
if(get_field('add_contact_block')) {
	include ( 'part-contact.php' );
}


if(get_field('add_contact_cta_block')) {
	include ( 'part-contact-cta.php' );
}



if(get_field('add_gift_block')) {
	$make_gift = array(
  	'background_color' => 'yellow',
	);
	include ( 'part-gift.php' );
}

?>

<?php endwhile; else: ?>
<?php endif; ?>

<?php get_footer(); ?>