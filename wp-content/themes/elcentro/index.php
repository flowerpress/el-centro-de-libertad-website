<?php get_header();


$hero_mobile_image = get_field('hero_image_mobile', get_option('page_for_posts')); 
$hero_desk_image = get_field('hero_image', get_option('page_for_posts'));

  if( !empty($hero_mobile_image) ): ?>
    <style>
      .subpage_hero {
        background-image:url('<?php echo $hero_mobile_image['url']; ?>');
      }
      @media (min-width: 750px) {
        .subpage_hero {
          background-image:url('<?php echo $hero_desk_image['url']; ?>');
        }
      }
    </style>
  <?php 
  
  else: ?>
  
    <style>
      .subpage_hero {
        background-image:url('<?php echo $hero_desk_image['url']; ?>');
      }
    </style>  
  
  <?php 
  endif; ?>



<!-- intro content -->
<?php if(get_field('intro_text', get_option('page_for_posts'))) { ?>
<div class="container container--margin-inner intro-container container--bgr-blue">
  <div class="container__content container__content--540">  
    <?php echo get_field('intro_text', get_option('page_for_posts')); ?>
  </div>
</div>
<?php } ?>

<!-- begin content -->
<section class="container container--bgr-<?php echo get_field('news_background_color', get_option('page_for_posts')); ?>">
	<div class="container__content container__news container__content--620">

		 <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		<article role="article" <?php post_class('editable_content') ?> id="post-<?php the_ID(); ?>">
		
		    <h1><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1>
		    <p class="post_author">Posted by: <strong><?php the_author(); ?></strong> on: <strong><?php the_time('m.d.Y') ?></strong></p>
		        <?php the_content(); ?>
		    
		    <footer>
		    	<p class="post-comments"><?php comments_popup_link(__('Comments (0)'), __('Comments (1)'), __('Comments (%)')); ?></p>
		    	<p> <?php the_category(', ') ?></p>
		    	<p><?php the_tags(""); ?></p>
		    </footer>
		</article>
		<?php comments_template(); // Get wp-comments.php template ?>
		<?php endwhile; else: ?>
		<p>
		  <?php _e('Sorry, no posts matched your criteria.'); ?>
		</p>
		<?php endif; ?>
		
		<p class="blog-pagination">
		  <?php posts_nav_link(' &#8212; ', __('&lt; Newer Posts'), __('Older Posts &gt;')); ?>
		</p>
	
	</div>

</section>


<?php
if(get_field('add_contact_block', get_option('page_for_posts'))) {
	include ( 'part-contact.php' );
}
?>

<?php
if(get_field('add_contact_cta_block', get_option('page_for_posts'))) {
	include ( 'part-contact-cta.php' );
}
?>

<?php
if(get_field('add_gift_block', get_option('page_for_posts'))) {
	$make_gift = array(
  	'background_color' => 'yellow',
	);
	include ( 'part-gift.php' );
}
?>
<!-- end content -->

<?php get_footer(); ?>