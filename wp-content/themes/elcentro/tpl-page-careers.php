<?php get_header();
/**
 * Template Name: Careers Page
 *
 * A custom page template.
 * @package WordPress
 * @subpackage FPCS
 */
?> 
<?php
  $introBG = get_field_object('intro_background_color');
  $hero_desk_image = get_field('hero_image');
  if( !empty($hero_desk_image) ):
	  include ( 'part-careers_subpage_hero.php' );
	endif;
?>
 
<!-- begin content -->
<div class="container container--margin-inner intro-container container--bgr-<?php echo $introBG['value']; ?>">
  <div class="container__content container__content--660">  
    <?php echo get_field('intro_text'); ?>
  </div>
</div>

<?php $noJobsMessage = get_field('no_current_jobs_open_text');
	  $viewCareerButtonText = get_field('view_career_button_text'); ?>

<!-- begin content -->
<section class="container container--bgr-yellow">
	<div class="container__content container__content--short">
  <h2><?php echo get_field('career_opening_headline') ?></h2>
		<?php $loop = new WP_Query( array( 'post_type' => 'jobs', 'posts_per_page' => 10 ) );

        if ( $loop->have_posts() ):

            while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <article role="article" <?php post_class('editable_content') ?> id="post-<?php the_ID(); ?>">
		            <h4><?php the_title(); ?></h4>
					<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">
					<h6><?php echo $viewCareerButtonText ?> ></h6>
				</a>
			</article>
            <?php endwhile;

        else: ?>
            <p style="text-align: center; font-size:20px;"><?php _e( $noJobsMessage ); ?></p>
        <?php endif;

        wp_reset_query();

        ?>
		
		
		
	</div>
</section>
<!-- end content -->
<?php // Check for Gift or Contact global modules
  
if(get_field('add_contact_block')) {
	include ( 'part-contact.php' );
}

if(get_field('add_contact_cta_block')) {
	include ( 'part-contact-cta.php' );
}

if(get_field('add_gift_block')) {
	$make_gift = array(
  	'background_color' => 'yellow',
	);
	include ( 'part-gift.php' );
}
?>

<?php get_footer(); ?>