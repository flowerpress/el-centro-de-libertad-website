<?php
/**
 * Template Name: Home Page
 *
 * A custom page template.
 * @package WordPress
 * @subpackage Bitclone Boilerplate
 */

 get_header();


 ?>


<?php
  $slider_settings = get_field('home_slides'); 
  include ( 'part-slider.php' );
?>


<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
 
  $blocks = get_field('page_content');
  if ( is_array($blocks) ) :
    foreach ($blocks as $block_key => $block) :
  
      switch ($block['acf_fc_layout']) :
  

        case 'page_intro':
			
          include ( 'part-intro.php' );
          break;
  
       
  
        case 'quote':
          $quote_settings = array(
            'quote_text' => $block['quote_text'],
            'quote_cite' => $block['quote_cite'],
          );
			
          include ( 'part-quote.php' );
          break;
          endswitch;
    endforeach;
  endif; 




?>

<?php endwhile; endif; ?>


<?php 
 $args = array( 
 'orderby' => 'date',
 'posts_per_page' => 1,
 );
 $the_query = new WP_Query( $args );
?>
<section id="news-homepage-container" class="container container--bgr-light-gray">
    <h2>The Latest</h2>
    <div class="latest-news-wrapper">
    <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <article role="article" class="article-wrapper" <?php post_class('editable_content') ?> id="post-<?php the_ID(); ?>">
            <h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
            <p class="post_author">Posted by: <strong><?php the_author(); ?></strong> on: <strong><?php the_time('m.d.Y') ?></strong></p>
            <?php the_post_thumbnail(null, 'medium' ); ?>
            
            <p><a href="<?php get_post_permalink(the_permalink()) ?>" class="cta-button">Read Article</a></p>
            
        </article>

    <?php endwhile; else: ?> <p>Sorry, there are no posts to display</p> <?php endif; ?>
    <?php wp_reset_query(); ?>
    </div>
</section>

<?php 
$blocks = get_field('page_content');
  if ( is_array($blocks) ) :
    foreach ($blocks as $block_key => $block) :
  
      switch ($block['acf_fc_layout']) :
 case 'our_programs':
          $programs_settings = array(
              'programs_title' => $block['programs_title'],
              'programs_description' => $block['programs_description'],
              'cta_link' => $block['cta_link'],
              'cta_title' => $block['cta_title'],
            );
          include ( 'part-our_programs.php' );
          break;
      endswitch;
    endforeach;
  endif; 

?>

<?php // Check for Gift or Contact global modules
	$make_gift = array(
  	'background_color' => 'yellow',
	);
	include ( 'part-gift.php' );
?>

<?php 
// get_template_part( 'part-popup' );
?>
	
<?php get_footer();
?>